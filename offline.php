 <?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.system
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
 require_once JPATH_ADMINISTRATOR . '/components/com_users/helpers/users.php';
$app = JFactory::getApplication();
$doc    = JFactory::getDocument();
 $twofactormethods = UsersHelper::getTwoFactorMethods();

$doc->addStyleSheet( 'templates/' . $this->template . '/css/metro.css', $type = 'text/css');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <jdoc:include type="head" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/offline.css" type="text/css" />
    <?php if ($this->direction == 'rtl') : ?>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/offline_rtl.css" type="text/css" />
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
</head>
<body>
    <jdoc:include type="message" />
    <div id="frame" class="outline" style="font-size: 12px;">
        <?php if ($app->getCfg('offline_image')) : ?>
        <img src="<?php echo $app->getCfg('offline_image'); ?>" alt="<?php echo htmlspecialchars($app->getCfg('sitename')); ?>" />
        <?php endif; ?>
        <h1>
            <?php echo htmlspecialchars($app->getCfg('sitename')); ?>
        </h1>
        <?php if ($app->getCfg('display_offline_message', 1) == 1 && str_replace(' ', '', $app->getCfg('offline_message')) != '') : ?>
        <p>
            <?php echo $app->getCfg('offline_message'); ?>
        </p>
        <?php elseif ($app->getCfg('display_offline_message', 1) == 2 && str_replace(' ', '', JText::_('JOFFLINE_MESSAGE')) != '') : ?>
        <p>
            <?php echo JText::_('JOFFLINE_MESSAGE'); ?>
        </p>
        <?php  endif; ?>
        <form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="form-login" role="form">
            <div class="form-group">
                <label for="username"><?php echo JText::_('JGLOBAL_USERNAME') ?></label>
                <input name="username" id="username" type="text" class="form-control" alt="<?php echo JText::_('JGLOBAL_USERNAME') ?>" size="18" />
            </div>
            <div class="form-group">
                    <label for="passwd"><?php echo JText::_('JGLOBAL_PASSWORD') ?></label>
                    <input type="password" name="password" class="form-control" size="18" alt="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" id="passwd" />
            </div>
            <?php if (count($twofactormethods) > 1) : ?>
            <div class="form-group">
                    <label for="secretkey"><?php echo JText::_('JGLOBAL_SECRETKEY'); ?></label>
                    <input type="text" name="secretkey" class="form-control" size="18" alt="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>" id="secretkey" />
            </div>
            <?php endif; ?>

            <input type="submit" name="Submit" class="btn btn-primary" value="<?php echo JText::_('JLOGIN') ?>" />
            <input type="hidden" name="option" value="com_users" />
            <input type="hidden" name="task" value="user.login" />
            <input type="hidden" name="return" value="<?php echo base64_encode(JURI::base()) ?>" />
            <?php echo JHtml::_('form.token'); ?>
        </form>
    </div>
</body>
</html>
