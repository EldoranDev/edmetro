<?php

defined('_JEXEC') or die;
if (!isset($this->error))
{
    $this->error = JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
    $this->debug = false;
}
//get language and direction
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

$doc->addStyleSheet( 'templates/' . $this->template . '/css/metro.css', $type = 'text/css');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<title><?php echo $this->error->getCode(); ?> - <?php echo $this->title; ?></title>
	
                        <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/metro.css" type="text/css" />
            <?php if ($this->direction == 'rtl') : ?>
	<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/system/css/error_rtl.css" type="text/css" />
			
            <?php endif; ?>
	<?php
		$debug = JFactory::getConfig()->get('debug_lang');
		if (JDEBUG || $debug)
		{
	?>
		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/media/cms/css/debug.css" type="text/css" />
	<?php
		}
	?>
                <style>
                    body{
                        background-color: #f5f5f5;
                    }
                    
                     .jumbotron {
                        margin: 80px 0;
                        text-align: center;
                        font-size: 14px;
                        background: white;
                        padding: 19px 29px 29px;
                        border: 1px solid #e5e5e5;
                         -webkit-border-radius: 5px;
                             -moz-border-radius: 5px;
                                       border-radius: 5px;
                    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                        -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                                  box-shadow: 0 1px 2px rgba(0,0,0,.05);
                      }
                      
                      .jumbotron h1 {
                        font-size: 100px;
                        line-height: 1;
                      }
                      
                      .jumbotron .lead {
                        font-size: 24px;
                        line-height: 1.25;
                      }
                </style>
</head>
<body>
    <div class="container">
        <div class="jumbotron">
            <h1><?php echo $this->error->getCode(); ?></h1>
            <p class="lead"><?php echo $this->error->getMessage(); ?></p>
            <div class="row" style="margin-top: 60px;">
                <div class="col-md-4">
                    <strong><?php echo JText::_('TPL_EDMETRO_ERROR_REASON'); ?></strong>
                    <small>
                    <ol>
                         <li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
                         <li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
                         <li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
                         <li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
                         <li><?php echo JText::_('JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND'); ?></li>
                         <li><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></li>
                     </ol>
                    </small>
                </div>
                <div class="col-md-3">
                    <p><strong><?php echo JText::_('JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES'); ?></strong></p>
                    <ul>
                        <li><a href="<?php echo $this->baseurl; ?>/index.php" title="<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>"><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="search navbar-search pull-right">
                        <strong><?php echo JText::_('TPL_EDMETRO_ERROR_TRY_SEARCH'); ?></strong>
                        <br />
                        <form action="/" method="post" class="form-inline" role="form">
                            <div class="form-group">
                                <label for="mod-search-searchword" class="sr-hidden"> </label> 
                            </div>
                            <div class="form-group">
                                <input name="searchword" id="mod-search-searchword" maxlength="20"  class="form-control search-query" type="text" size="20" value="Suchen..."  onblur="if (this.value=='') this.value='Suchen...';" onfocus="if (this.value=='Suchen...') this.value='';" />    	
                                <input type="hidden" name="task" value="search" />
                                <input type="hidden" name="option" value="com_search" />
                                <input type="hidden" name="Itemid" value="140" />
                            </div>
                            <div class="form-group">
                                <input type="submit" class="form-control" name="submit" value="Suchen!" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="alert alert-info" style="margin-top: 35px;">
                <p><strong><?php echo JText::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?></strong></p>
                <p><?php echo $this->error->getMessage(); ?></p>
                        <p>
                            <?php if ($this->debug) :
                            echo $this->renderBacktrace();
                            endif; ?>
                        </p>
            </div>
        </div>
    </div>
</body>
</html>
