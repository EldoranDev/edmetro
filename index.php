<?php
    defined('_JEXEC') or die;
    JHtml::_('behavior.framework', true);
    
    $app    = JFactory::getApplication();
    $doc    = JFactory::getDocument();
    
    $showMenu           = ($this->countModules('position-1'));
    $showHeroUnit       = ($this->countModules('position-2'));
    $showPostHeroUnit   = ($this->countModules('position-3'));
    $showMenuLeft       = ($this->countModules('position-4'));
    $showMenuRight     = ($this->countModules('position-5'));
    $showFooter         = ($this->countModules('position-10'));
    $contentSize          = 12;
    
    $contentSize = ($showMenuLeft) ? $contentSize - 2 : $contentSize;
    $contentSize = ($showMenuRight) ? $contentSize - 2 : $contentSize;
    
    $ga_Code            = $this->params->get('ga_code','');
    
    //$doc->addStyleSheet(JURI::base() . 'templates/system/css/system.css');
    $doc->addStyleSheet('templates/' . $this->template . '/css/metro.css');
    $doc->addStyleSheet('templates/' . $this->template . '/css/lightbox.css');
    $doc->addStyleSheet('templates/' . $this->template . '/css/EDMetro.css');
    JHtml::_('bootstrap.framework');
    $doc->addScript('templates/' . $this->template . '/js/lightbox-2.6.min.js');
    $doc->addScript('templates/' . $this->template . '/js/edmetro.js');
?>  

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<jdoc:include type="head" />
    </head>
    <body>
        <div id="wrap">
            <?php if($showMenu): ?>
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php"><?= $app->getCfg('sitename') ?></a>
                    </div>
                    <div class="collapse navbar-collapse navbar-ex1-collaps"> 
                        <jdoc:include type="modules" name="position-1" />
                    </div>
                </div>
            </nav>
            <?php endif; ?>

            <?php if($showHeroUnit) : ?>
            <div class="jumbotron">
                <div class="container">
                    <jdoc:include type="modules" name="position-2" />
                </div>
            </div>

            <?php endif; ?>

            <?php if($showPostHeroUnit) : ?>
            <div class="container">
                <jdoc:include type="modules" name="position-3" />
            </div>
            <?php endif; ?>

            <div class="container" style="padding-top: 10px; ">
                <div class="row">
                    <?php if($showMenuLeft) : ?>
                    <div class="col-md-2">
                        <div class="well sidebar-nav">
                            <jdoc:include type="modules" name="position-4" style="xhtml"/>
                        </div>
                    </div>
                     <?php endif; ?>
                    <div class="col-md-<?php echo $contentSize ?>" >
                        <jdoc:include type="message" />
                        <jdoc:include type="component" />
                    </div>
                    <?php if($showMenuRight):?>
                    <div class="col-md-2 well sidebar-nav" style="margin-top: 30px;">
                        <jdoc:include type="modules" name="position-5" style="xhtml"/>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        
        </div>
        
        <?php if($showFooter) : ?>
        
        <div id="footer">
            <hr>
            <div class="container">
              <jdoc:include type="modules" name="position-10" />
            </div>
         </div>
        <?php endif; ?>
        
        <!-- Google Analytics -->
    <?php if($ga_Code !== '') : ?>
        <script>
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', '<?= $ga_Code ?>']);
            _gaq.push(['_trackPageview']);

            (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>

    <?php endif; ?>
    </body>
</html>