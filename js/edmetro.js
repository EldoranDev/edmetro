// LightBox 2 for Thumbnails
// Wrap Link around Thumbnails

/*
 * class=thumbnail
 * title=img.alt
 * href=img.src
 * data-lightbox=img.title
 */
jQuery(document).ready(function(){
    jQuery('div.row img').each(function(){
       jQuery(this).wrap('<a class="thumbnail col-md-3" style="margin-right: 2px; margin-left: 2px;" title="' + jQuery(this).attr('alt') + '" href="' + jQuery(this).attr('src') + '" data-lightbox="' + jQuery(this).attr('title') + '"></a>');
       jQuery(this).removeClass('col-md-3');
    });
});
